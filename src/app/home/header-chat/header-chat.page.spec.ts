import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderChatPage } from './header-chat.page';

describe('HeaderChatPage', () => {
  let component: HeaderChatPage;
  let fixture: ComponentFixture<HeaderChatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderChatPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderChatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
