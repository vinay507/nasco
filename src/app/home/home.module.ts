import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { HeaderChatPage } from './header-chat/header-chat.page';
import { SideBarPage } from './side-bar/side-bar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([{
      path: '', component: HomePage,
      children: [
        {
          path: '', redirectTo: 'message', pathMatch: 'full'
        },
        {
          path: 'message', loadChildren: './message/message.module'
        },

      ]
    }])
  ],
  declarations: [HomePage, HeaderChatPage, SideBarPage]
})
export class HomePageModule { }
