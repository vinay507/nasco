import { Component, OnInit } from '@angular/core';
import { EventDispatchService } from './services/eventdispach.service';
import { Message } from './models';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-message',
  templateUrl: './message.page.html',
  styleUrls: ['./message.page.scss'],
})
export class MessagePage {
  DetailsEntered: boolean = true;
  public message: Message;
  public messages: Message[] = [];
  subscription: Subscription;

  constructor(public eventService: EventDispatchService) {
 
    // this.messages = [
    //   // new Message('Welcome to Nasco chatbot', 'assets/images/bot.png', new Date())
    // ];
  }

  ionViewDidLoad() {
    this.message = new Message('', 'assets/images/user.png');
    this.subscription = this.eventService.listen().subscribe((e: any) => {
      if (e.type == 'RefreshChat') {
        // console.log('refreshdone');
        this.message = new Message('', 'assets/images/user.png');
        this.messages = [];
      }
    });
  }
  ButtonAction(text) {
    this.DetailsEntered = false;
    //console.log('button-Action', text);
    const evnt = document.createEvent('CustomEvent');
    evnt.initCustomEvent('UserSelection', true, true, text);
    this.eventService.dispatch(evnt);
  }
}
