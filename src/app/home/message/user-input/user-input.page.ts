import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Message } from '../models';
import { DialogFlowService } from '../services/dialog-flow.service';
import { HttpClient } from '@angular/common/http';
import { EventDispatchService } from '../services/eventdispach.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';

@Component({
  selector: 'app-user-input',
  templateUrl: './user-input.page.html',
  styleUrls: ['./user-input.page.scss'],
})
export class UserInputPage implements OnInit {

  @Input('message') message: Message;

  @Input('messages') messages: Message[] = [];
  constructor(private dialogFlowService: DialogFlowService,
    public eventService: EventDispatchService,
    private camera: Camera,
    private speechRecognition: SpeechRecognition,
    private http: HttpClient) {
    this.eventService.listen().subscribe((event: any) => {
      if (event.type == "UserSelection") {
        this.message.content = event.detail;
        this.sendMessage();
      }
    })
  }

  ngOnInit() {
    this.messageDelivery('hi');
  }

  public sendMessage(): void {
    if (this.message.content.trim() != "") {
      if (this.message.content.indexOf('@') != -1) {
        // console.log('email-->', this.message.content);
        const data = { 'email': this.message.content.split(':')[1] };
        this.http.post('/emailSending', data).subscribe(res => {
          // console.log('response', res);
          this.message.timestamp = new Date();
          this.message.msgFrom = 'user';
          this.messages.push(this.message);
          this.messageDelivery(this.message.content);
        });
      } else {
        if (this.message.content.includes('Third Party')) {
          sessionStorage.setItem('PolicyType', 'Third_Party');
        } else if (this.message.content.includes('Comprehensive')) {
          sessionStorage.setItem('PolicyType', 'Comprehensive');
        }
        for (let j = 0; j < this.messages.length; j++) {
          // console.log('srhjdfgj')
          if (this.messages[j].content.content) {
            for (let i = 0; i < this.messages[j].content.content.length; i++) {
              // console.log('srhjdfgj', this.messages[j].content.content)
              if (this.messages[j].content.content[i] && this.messages[j].content.content[i].type == 'buttons') {
                this.messages[j].content.content[i] = '';
                // const evnt = document.createEvent('CustomEvent');
                // evnt.initEvent('SubmitedImage', true, true);
                // this.eventService.dispatch(evnt);
              }
            }
          }
        }
        this.message.timestamp = new Date();
        this.message.msgFrom = 'user';
        this.messages.push(this.message);
        this.messageDelivery(this.message.content);
      }
    }
  }


  mic() {
    // Check feature available
    this.speechRecognition.isRecognitionAvailable()
      .then((available: boolean) => console.log(available))

    // Start the recognition process
    // this.speechRecognition.startListening()
    //   .subscribe(
    //     (matches: string[]) => console.log(matches),
    //     (onerror) => console.log('error:', onerror)
    //   )

    // // Stop the recognition process (iOS only)
    // this.speechRecognition.stopListening()

    // Get the list of supported languages
    this.speechRecognition.getSupportedLanguages()
      .then(
        (languages: string[]) => console.log(languages),
        (error) => console.log(error)
      )

    // Check permission
    this.speechRecognition.hasPermission()
      .then((hasPermission: boolean) => console.log(hasPermission))

    // Request permissions
    this.speechRecognition.requestPermission()
      .then(
        () => console.log('Granted'),
        () => console.log('Denied')
      )
  }
  messageDelivery(msg) {
    this.dialogFlowService.getAnswerOfBot(msg).subscribe((res: any) => {
      if (res.result.fulfillment.speech) {
        this.messages.push(
          new Message(res.result.fulfillment.speech, 'assets/images/bot.png', res.timestamp));
      } else {
        for (let k = 0; k < res.result.fulfillment.messages.length; k++) {
          if (res.result.fulfillment.messages[k].payload) {


            const data = res.result.fulfillment.messages[k].payload;
            // console.log('response data-->' + JSON.stringify(res.result.fulfillment.messages[0].payload))
            const datares: any = {
              type: res.result.fulfillment.messages[k].payload.type,
              content: []
            }
            this.messages.push(
              new Message(datares, 'assets/images/bot.png', res.timestamp)
            );
            // console.log('data pushed');
            let Time = 1000;
            for (let i = 0; i < data.content.length; i++) {
              if (data.content[i].type == 'p') {
                if (data.content[i].multiple) {
                  res.result.fulfillment.messages[k].payload.content[i].randNum = Math.floor(Math.random() * data.content[i].text.length);
                }
              }
              // if (datares.type == 'inputEntry' && data.content[i].type == 'inputValue') {
              //   this.message.content = data.content[i].text;
              // }

              setTimeout(() => {

                this.messages[this.messages.length - 1].content.content.push(
                  res.result.fulfillment.messages[k].payload.content[i]
                );
                // const evnt = document.createEvent('CustomEvent');
                // evnt.initEvent('messageAdded', true, true);
                // this.eventService.dispatch(evnt);
                // // console.log('array', res.result.fulfillment.messages[0].payload.content[i], new Date().getTime());
                // if (i == data.content.length - 1) {
                //   const evnt = document.createEvent('CustomEvent');
                //   evnt.initEvent('LoadingComplete', true, true);
                //   this.eventService.dispatch(evnt);

                // }
              }, Time);

              let text = '';
              if (data.content[i].text && data.content[i].multiple) { text = data.content[i].text[0] }
              else if (data.content[i].text) { text = data.content[i].text; }
              Time = Time + (500 * (text.split(' ').length / 5));
              // console.log(text, text.split(' ').length)
            }

          }
        }
      }

    });
    this.message = new Message('', 'assets/images/user.png');
  }
  captureImage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }

}

