import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment'
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { NgZone } from '@angular/core';
import { from } from 'rxjs';
declare var ApiAIPromises: any;
@Injectable({
  providedIn: 'root'
})
export class DialogFlowService {
  private baseURL: string = "https://api.dialogflow.com/v1/query?v=20150910";
  private token: string = environment.token;
  private sessiontime;
  constructor(private http: HttpClient,
    private platform: Platform,
    private splashScreen: SplashScreen,
    public ngZone: NgZone
  ) {
    this.sessiontime = 10000000 + Math.floor(Math.random() * 999999999);
    // console.log('randomValue',this.sessiontime)

    if (this.platform.is('cordova')) {
      this.initializeApp();
    }
  }

  public getResponse(query: string) {
    let data = {
      query: query,
      lifespanCount: '99999',
      lang: 'en',
      sessionId: this.sessiontime + ''
    }

    return this.http
      .post(`${this.baseURL}`, data, this.getHeaders())
  }

  initializeApp() {
    this.platform.ready().then(() => {
      ApiAIPromises.init({
        clientAccessToken: "d1342f8f4c3c40a5acfad69517eb9f06",//"ebc4f337648141f3a23956012f2dbe18"
      }).then(result => console.log(result));
      this.splashScreen.hide();
      // a6ab914ee190493a9a3d0843fa5f75c4
    });
  }
  ask(question) {
    return from(ApiAIPromises.requestText({
      query: question
    }).then((resp) => {
      return resp
      // this.ngZone.run(() => {
      //   this.answers.push(fulfillment);
      // });
    }))
  }
  getAnswerOfBot(question: string) {
    if (this.platform.is('cordova')) {
      return this.ask(question);
    } else {
      return this.getResponse(question);
    }
  }
  public getHeaders() {
    let headerstest;
    // headers.append('Authorization', `Bearer ${this.token}`);
    headerstest = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${this.token}`

      })
    };
    return headerstest;
  }
}