
/** Angular imports */
import { Injectable } from '@angular/core';

/** Rxjs imports */
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';

@Injectable({
    providedIn:'root'
})
/** This service is used to track all the popup events and dispatch click events */
export class EventDispatchService {

        constructor() { }

        trackEvnt: Subject<Event> = new Subject<Event>();

        // Below method used to dispatch events.
        dispatch(data: Event) {
                this.trackEvnt.next(data);
        }

        // Below method is used to listen to the dispatched events.
        // We have to use .subscribe() to track the method success callback in calledcall.
        listen(): Observable<Event> {
                return this.trackEvnt.asObservable();
        }
}