import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Message } from '../models';

@Component({
  selector: 'app-user-message',
  templateUrl: './user-message.page.html',
  styleUrls: ['./user-message.page.scss'],
})
export class UserMessagePage implements OnInit {

  @Input('message') message: Message;
  constructor() { }

  ngOnInit() {
  }

}
