import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotoCardPage } from './photo-card.page';

describe('PhotoCardPage', () => {
  let component: PhotoCardPage;
  let fixture: ComponentFixture<PhotoCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotoCardPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
