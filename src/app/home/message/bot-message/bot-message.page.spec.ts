import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BotMessagePage } from './bot-message.page';

describe('BotMessagePage', () => {
  let component: BotMessagePage;
  let fixture: ComponentFixture<BotMessagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BotMessagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BotMessagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
