import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CameraCardPage } from './camera-card.page';

describe('CameraCardPage', () => {
  let component: CameraCardPage;
  let fixture: ComponentFixture<CameraCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CameraCardPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CameraCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
