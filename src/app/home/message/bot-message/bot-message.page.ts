import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Message } from '../models';
import { EventDispatchService } from 'src/app/home/message/services/eventdispach.service';
import { SimpleChanges } from '@angular/core';
import { Camera } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-bot-message',
  templateUrl: './bot-message.page.html',
  styleUrls: ['./bot-message.page.scss'],
})
export class BotMessagePage implements OnInit {
  DetailsEntered = true;

  @Input('message') message: Message;
  constructor(public eventService: EventDispatchService,
    private camera: Camera, ) {
    this.DetailsEntered = true;
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.message && changes.message.currentValue.content.type === 'scanCard') {

      this.camera.getPicture().then((imageData) => {
        // console.log(changes.message.currentValue.content.type);
      });
    }

  }
  ngOnInit() {
  }

}
