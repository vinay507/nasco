import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenceCardPage } from './licence-card.page';

describe('LicenceCardPage', () => {
  let component: LicenceCardPage;
  let fixture: ComponentFixture<LicenceCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenceCardPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenceCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
