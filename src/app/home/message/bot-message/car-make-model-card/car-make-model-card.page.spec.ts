import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarMakeModelCardPage } from './car-make-model-card.page';

describe('CarMakeModelCardPage', () => {
  let component: CarMakeModelCardPage;
  let fixture: ComponentFixture<CarMakeModelCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarMakeModelCardPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarMakeModelCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
