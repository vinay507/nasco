import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullCarDetailsCardPage } from './full-car-details-card.page';

describe('FullCarDetailsCardPage', () => {
  let component: FullCarDetailsCardPage;
  let fixture: ComponentFixture<FullCarDetailsCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FullCarDetailsCardPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullCarDetailsCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
