import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MessagePage } from './message.page';
import { BotMessagePage } from './bot-message/bot-message.page';
import { UserMessagePage } from './user-message/user-message.page';
import { UserInputPage } from './user-input/user-input.page';
import { CameraCardPage } from './bot-message/camera-card/camera-card.page';
import { CarMakeModelCardPage } from './bot-message/car-make-model-card/car-make-model-card.page';
import { FullCarDetailsCardPage } from './bot-message/full-car-details-card/full-car-details-card.page';
import { LicenceCardPage } from './bot-message/licence-card/licence-card.page';
import { PhotoCardPage } from './bot-message/photo-card/photo-card.page';

const routes: Routes = [
  {
    path: '',
    component: MessagePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    MessagePage,
    BotMessagePage,
    UserMessagePage,
    UserInputPage,
    PhotoCardPage,
    LicenceCardPage,
    FullCarDetailsCardPage,
    CarMakeModelCardPage,
    CameraCardPage
  ]
})
export class MessagePageModule { }
