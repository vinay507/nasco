export class Message {
    content: any;
    timestamp: Date;
    avatar: string;
    msgFrom:string;
  
    constructor(content: any, avatar: string, timestamp?: Date,msgFrom?: string ){
      this.content = content;
      this.timestamp = timestamp;
      this.avatar = avatar;
      this.msgFrom=msgFrom;
    }
  }
  