import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Network } from '@ionic-native/network/ngx';
import * as $ from '../assets/script/jquery.min.js';
import { ToastController } from '@ionic/angular';



@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  splash_screen = true;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private network: Network,
    public toastController: ToastController
  ) {
    this.initializeApp();
    Promise.resolve().then(() => {

      setTimeout(() => {
        $('.splash_logo').css("opacity", "0.8");
      }, 6000);
      setTimeout(() => {
        $('.splash_logo').css("opacity", "0.7");
      }, 6100);
      setTimeout(() => {
        $('.splash_logo').css("opacity", "0.6");
      }, 6200);
      setTimeout(() => {
        $('.splash_logo').css("opacity", "0.5");
      }, 6300);
      setTimeout(() => {
        $('.splash_logo').css("opacity", "0.4");
      }, 6400);
      setTimeout(() => {
        $('.splash_logo').css("opacity", "0.3");
      }, 6500);
      setTimeout(() => {
        $('.splash_logo').css("opacity", "0.2");
      }, 6600);
      setTimeout(() => {
        $('.splash_logo').css("opacity", "0.1");
      }, 6700);
      setTimeout(() => {
        $('.splash_logo').css("opacity", "0");
      }, 6800);
      setTimeout(() => {
        $('.splash_logo').css("display", "none");
      }, 7000);
    }).then(() => {
      setTimeout(() => {
        // console.log('ended');
        this.splash_screen = false;
        $("body").removeClass("Splash-screen-2");
        $(".Splash-screen-2").remove();
        $('.horizonSVG').css("display", "none");
        $('.devicesSVG').css("display", "none");
      }, 7000);

    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.checkNetwork();
    });   
  }
  
  async checkNetwork() {
    // watch network for a connection
    this.network.onConnect().subscribe(() => {
      // alert('network connected!');
      this.showToast('Network Connected!', 'success');
      // We just got a connection but we need to wait briefly
      // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.
      setTimeout(() => {
        if (this.network.type === 'wifi') {
          // alert('we got a wifi connection, woohoo!');
         this.showToast('We got a wifi connection, woohoo!', 'success');
          
        }
      }, 3000);
    }, (error)=>{
      this.showToast('Network connect Required', 'Error');
    });
  }

  async showToast(msg, type){
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      position: 'top',
      color: type
    });
    toast.present();
  }

}
